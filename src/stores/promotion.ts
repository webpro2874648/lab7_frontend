import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import { useLoadingStore } from './loading'
import promotionService from '@/services/promotion'
import type { Promotion } from '@/types/Promotion'
export const usePromotionStore = defineStore('promotion', () => {
  const loadingStore = useLoadingStore()
  const promotions = ref<Promotion[]>([])

async function deletePromotion(promotion: Promotion) {
  loadingStore.doLoad()
  const res = await promotionService.delPromotion(promotion)
  await getPromotions()
  loadingStore.finish()
}

async function savePromotion(promotion: Promotion) {
  loadingStore.doLoad()
  if (promotion.id < 0) { //add new
    const res = await promotionService.addPromotion(promotion)
  } else { //update
    const res = await promotionService.updatePromotion(promotion)
  }
  await getPromotions()
  loadingStore.finish()
}

//data function
async function getPromotions() {
  loadingStore.doLoad()
  const res = await promotionService.getPromotions()
  promotions.value = res.data
  loadingStore.finish()
}
async function getPromotion(id:number) {
  loadingStore.doLoad()
  const res = await promotionService.getPromotion(id)
  promotions.value = res.data
  loadingStore.finish()
}


  return { deletePromotion, savePromotion, getPromotion, getPromotions, promotions }
})
